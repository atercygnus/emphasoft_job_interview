import re
from typing import List, Dict

import pandas as pd
import numpy as np
import requests

from xml.etree import ElementTree

REQUEST_STRING = "https://sdw-wsrest.ecb.europa.eu/service/data/BP6/{tr_id}?detail=dataonly" 
DEFAULT_NAMESPACE = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic"

def get_transactions(identifier: str) -> pd.DataFrame:

    """ 
        Fetches OBS_VALUES of a single transaction 

        Args: 
            identifier (str): transaction id. 

        Returns: 
            Dataframe, containing OBS_VALUES of a transaction through all time periods.             

    """

    response = requests.get(REQUEST_STRING.format(tr_id=identifier))

    if not response.ok:
        raise Exception(f'Could not retreive transaction data. Maybe wrong identifier?({identifier})')

    tree = ElementTree.fromstring(response.content)

    tr_list = []

    tree = ElementTree.fromstring(response.content)
    for obs in tree.findall('.//{{{namespace}}}Obs'.format(namespace=DEFAULT_NAMESPACE)):
	
        dim = obs.find('{{{namespace}}}ObsDimension'.format(namespace=DEFAULT_NAMESPACE)).attrib['value']
        val = obs.find('{{{namespace}}}ObsValue'.format(namespace=DEFAULT_NAMESPACE)).attrib['value']
	
        tr_list.append((dim, val))

    tr_df = pd.DataFrame(tr_list, columns = ['TIME_PERIOD', 'OBS_VALUE'])
    tr_df['IDENTIFIER'] = identifier
    tr_df = tr_df.astype({'IDENTIFIER': 'category', 'TIME_PERIOD': str, 'OBS_VALUE': float})


    return tr_df[['IDENTIFIER', 'TIME_PERIOD', 'OBS_VALUE']]


def parse_formula(formula: str) -> Dict:

    def groom_operand_list(parsed_formula: List) -> List:

        operands = [] 
        operations = ['+'] # assume first operand is always positive

        for elem in parsed_formula:

            if elem.strip() in ['+', '-']:
                operations.append(elem.strip())
            else:
                operands.append(elem.strip())

        return operands, operations

    pattern = r'([\+\-])'

    formula_split = formula.split('=')

    if len(formula_split) != 2:
        raise Exception('Wrong formula')

    result_tr = formula_split[0].strip()
    operand_trs, operations = groom_operand_list(re.split(pattern, formula_split[1]))

    return {'result_transaction_id': result_tr, 'operand_transactions_list': operand_trs, 'operation_list': operations}


def internal_get_formula_data(formula: str) -> pd.DataFrame:

    parsed_formula = parse_formula(formula)

    tr_list = [get_transactions(tr_id) for tr_id in parsed_formula['operand_transactions_list']]

    df_merged = pd.concat(tr_list).pivot(index='TIME_PERIOD', columns='IDENTIFIER', values='OBS_VALUE').fillna(0)

    return df_merged[[*parsed_formula['operand_transactions_list']]], parsed_formula


def get_formula_data(formula: str) -> pd.DataFrame:

    """ Fetches OBS values of transactions, present in the formula """

    tr_df, parsed_formula = internal_get_formula_data(formula)

    return tr_df


def compute_aggregates(formula: str) -> pd.DataFrame:

    
    """
	Computes transaction result according to formula

	Parameters:

            formula(str): formula to be computed. Supported operations: +, -. 
            Operands are transaction ids(ex. 'Q.N.I8.W1.S1P.S1.T.A.FA.D.F._Z.EUR._T._X.N')

	Returns: 

            Pandas Dataframe with a single column. 
            Name of the column taken from the left-hand side of the formula(left to the =). 
            Value of the column computed according to formula.

    """
   
    tr_df, parsed_formula = internal_get_formula_data(formula)

    tr_df[parsed_formula['result_transaction_id']] = 0

    for op, col in zip(parsed_formula['operation_list'], parsed_formula['operand_transactions_list']):

        if op == '+':
            tr_df[parsed_formula['result_transaction_id']] += tr_df[col]
        elif op == '-':
            tr_df[parsed_formula['result_transaction_id']] -= tr_df[col]
        else:
            raise Exception(f'Unknown operation: {op}')

    return tr_df[[parsed_formula['result_transaction_id']]]

 
if __name__ == '__main__':

    # Test cases here:

    #print(get_transactions('Q.N.I8.W1.S1.S1.T.A.FA.D.F._Z.EUR._T._X.N'))

    #print(get_formula_data('Q.N.I8.W1.S1.S1.T.A.FA.D.F._Z.EUR._T._X.N = Q.N.I8.W1.S1P.S1.T.A.FA.D.F._Z.EUR._T._X.N + Q.N.I8.W1.S1Q.S1.T.A.FA.D.F._Z.EUR._T._X.N'))

    print(compute_aggregates('Q.N.I8.W1.S1.S1.T.A.FA.D.F._Z.EUR._T._X.N = Q.N.I8.W1.S1P.S1.T.A.FA.D.F._Z.EUR._T._X.N + Q.N.I8.W1.S1Q.S1.T.A.FA.D.F._Z.EUR._T._X.N'))

